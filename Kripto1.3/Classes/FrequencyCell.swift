//
//  FrequencyCell.swift
//  Kripto1.3
//
//  Created by Odin on 11/4/14.
//  Copyright (c) 2014 Mordor. All rights reserved.
//

import UIKit

class FrequencyCell: UICollectionViewCell {
    
  @IBOutlet weak var numberLabel: UILabel!
  @IBOutlet weak var frequencyLabel: UILabel!
  @IBOutlet weak var subTitleLabel: UILabel!
}
